// Need to Pass the Parameter of the Time to the Function.
// Example: this.obj.Time = this.msToTime(this.obj.Time);

const msToTime(s) {
		let ms = s % 1000;
		s = (s - ms) / 1000;
		let secs = s % 60;
		s = (s - secs) / 60;
		let mins = s % 60;
		let hrs = (s - mins) / 60;
		hrs = hrs < 10 ? "0" + hrs : hrs;
		mins = mins < 10 ? "0" + mins : mins;
		this.obj.Hour = hrs;
		this.obj.Minute = mins;
		return hrs + ":" + mins + ":00.000Z";
	}